import http from "../node_modules/http";
import fs from "../node_modules/fs";

const port = process.env.PORT || 8080;
const authorsBase = [
    {
      "name": "Stephen King",
      "nationality": "american", "books": ["It", "The Stand", "Carrie"]
    },
    {
      "name": "J.K. Rowling",
      "nationality": "british", "books": ["Harry Potter", "Troubled Blood", "Lethal White"]
    },
    {
      "name": "Amy Tan",
      "nationality": "chinese-american", "books": ["El club de la buena estrella"]
    },
    {
      "name": "Tana French",
      "nationality": "american-irish", "books": ["El silencio del bosque", "En piel ajena", "Faithful Place"]
    },
    {
      "name": "Danzy Senna",
      "nationality": "american", "books": ["New People", "Symptomatic", "Nouveaux visages"]
    }
  ]

const server = http.createServer((req, res) => {
  var filePath = "./" + request.url;
  if (filePath == "./") {
    filePath = "./index.html";
  }

  const contentType = "text/html";

  fs.readFile(filePath, (error, content) => {
    if (error && error.code === "ENOENT") {
      fs.readFile("./404.html", (error, content) => {
        response.writeHead(404, { "Content-Type": contentType });
        response.end(content, "utf-8");
      });
    } else {
      response.writeHead(200, { "Content-Type": contentType });
      response.end(content, "utf-8");
    }
  });
  //Get
  if (req.url === '/books') {
    response.writeHead(200, { "Content-Type": "application/json" });
    response.end(
        JSON.stringify({
            books: authorsBase
        })
  );
   }

});

http.createServer((req, res) => {
    // If we are accessing the root, it should be redirected to the default language,
    // We shouldn't get a 404 page.
   
    if (req.url === '/') {
     req.url = `/${mainLocale}`
    }
    mount(req, res)
   }).listen(port, () => {
    console.log(`\x1B[32mServer running at http://localhost:${port}/${mainLocale}/\x1B[39m`)
   })

server.listen(port, () => {
  console.log(`Server running at port ${port}`);
});