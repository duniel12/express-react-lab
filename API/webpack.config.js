
import HtmlWebpackPlugin from './node_modules/html-webpack-plugin';
import MiniCssExtractPlugin from'./node_modules/mini-css-extract-plugin';
import path from'./node_modules/path';

export default {
  
  entry: './src/index.js',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname,'dist'),
    clean: true
  },
 
  plugins: [new HtmlWebpackPlugin({ title: "My Webpack app", template: "src/index.html" }), new MiniCssExtractPlugin()],
  devServer: { watchFiles: ["src/**/*.js", "src/**/*.html", "src/**/*.scss"] },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      
    ],
  },
  

};
