import React, { useState, useEffect } from 'react';
import { Link, Outlet } from 'react-router-dom';

import ObjectList from '../../components/ObjectList/ObjectList';
import AddInfo from '../../components/AddInfo/AddInfo';
import Modal from '../../components/Modal/Modal';
import Filter from '../../components/Filter/Filter';
import useModalShow from '../../components/Hooks/useModalShow';
//Hooks
import useBreakpoints from '../../components/Hooks/useBreakpoints';
import GoBack from '../../components/GoBack/GoBack';


const Authors = () => {
    const loadDelay = 1.5;
    const [author, setAuthor] = useState(null);
    const [show, setShow] = useState(true);
    const [showModalAuthor, setShowModalAuthor] = useState("");
    const [fetchData, setFectchData] = useState(false);

    //For filtering
    const [filter, setFilter] = useState("");

    //Debounce hook
    const [results, setResults] = useState([]);
    // Searching status (whether there is pending API request)
    const [isSearching, setIsSearching] = useState(false);

    let token = localStorage.getItem("JWT").replace(/^"(.+(?="$))"$/, '$1');
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };

    const [authors, setAuthors] = useState([]);

    const debouncedSearchTerm = useDebounce(filter, 500);

    const observable = useModalShow();
    const [breakPoint] = useBreakpoints();
    


    useEffect(
        () => {
            fetch("http://localhost:8000/authors", requestOptions).then((response) => response.json()).then((data) => {
                console.log("Authors fetch: ", data);
                setAuthors(data);
            })
            if(fetchData) {
                fetch("http://localhost:8000/authors", requestOptions).then((response) => response.json()).then((data) => {
                console.log("Authors fetch: ", data);
                setAuthors(data);
                
            })
            }

            if (debouncedSearchTerm) {
                console.log("debounce ")
                setIsSearching(true);
            } else {
                setResults([]);
                setIsSearching(false);
            }
            let timer1 = setTimeout(() => setShow(false), loadDelay * 1000);
            return () => {
                clearTimeout(timer1);
            };

        },
        [debouncedSearchTerm,fetchData]
    );

    //Shows info of authors
    const handleClick = (pauthor) => {
        if (pauthor === author) {
            setAuthor(null);
        } else {
            console.log("set author " , pauthor);
            setAuthor(pauthor);
        }

    }

    const FilterChange = (info) => {
        setFilter(useDebounce(info, 1000));
    }

    //Determines if its a new author click or an edit button
    //Info is the parameter for add or edit author
    const handleAuthor = (info) => {
        console.log(info)
        if (info === "Add author") {
            setShowModalAuthor("Add author");
        } else if (info === "Edit author") {
            setShowModalAuthor("Edit author");
        }
    }

    const handleCloseModal = (info) => {
        setShowModalAuthor(info);
    }

    const submitAuthor = (author, modal) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` },
            body: JSON.stringify(author)
        };
        fetch('http://localhost:8000/authors', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                console.log("Authors post made: ", data);
                setShowModalAuthor(modal);
                setFectchData(true);
            });

    }
    
    if(token.length === 0 || token === null || authors === "Session expired") {
        return(
            <div className="error__root">
                <h1>Not logged in</h1>
                <Link to={"/"}>Go home</Link>
            </div>
        );
    } else {
        return (
            <div>
                {show && <Modal addClose={false} show={show} />}
    
                {
                    //Show modal of add author
                    showModalAuthor === "Add author" &&
                    <Modal addClose={true} show={false} title={"Add author"} handleCloseModal={handleCloseModal} submitAuthor={submitAuthor} type={"author"} />
                }
                {
                    //Show modal of edit author
                    showModalAuthor === "Edit author" &&
                    <Modal addClose={true} show={false} title={"Edit author"} handleCloseModal={handleCloseModal} submitAuthor={submitAuthor} />
                }
                <div className="authors__root">
    
                    <div className="authors__info__btns">
                        <div className="authors__breakpoints">
                            {breakPoint.length > 0 && breakPoint[0]}
                        </div>
    
                        <h2 className="authors__title">Authors</h2>
                        <AddInfo info={"Add author"} handleClick={() => handleAuthor("Add author")} />
                        <Filter setFilter={setFilter} />
                        <ObjectList handleClick={handleClick} authors={authors} filter={filter} />
    
                        <GoBack />
                    </div>
                    
                    {author && <div className="authors__info__root">
                        <Outlet />
                    </div>}
                </div>
            </div>
        );
    }
    
}

// Hook
function useDebounce(value, delay) {
    // debounced value
    const [debouncedValue, setDebouncedValue] = useState(value);
    useEffect(
        () => {
            // Update debounced value after delay
            const handler = setTimeout(() => {
                setDebouncedValue(value);
            }, delay);
            return () => {
                clearTimeout(handler);
            };
        },
        [value, delay]
    );
    return debouncedValue;
}


export default Authors;
