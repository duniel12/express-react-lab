import React, { useState, useEffect } from 'react';
import { Outlet, Link } from 'react-router-dom';

import ObjectList from '../../components/ObjectList/ObjectList';
import AddInfo from '../../components/AddInfo/AddInfo';
import Modal from '../../components/Modal/Modal';
import Filter from '../../components/Filter/Filter';
import useModalShow from '../../components/Hooks/useModalShow';
import GoBack from '../../components/GoBack/GoBack';
//Hooks
import useBreakpoints from '../../components/Hooks/useBreakpoints';


const Books = () => {
    const loadDelay = 1.5;
    const [book, setBook] = useState(null);
    const [show, setShow] = useState(true);
    const [showModalBook, setShowModalBook] = useState("");
    const [fetchData, setFectchData] = useState(false);

    //For filtering
    const [filter, setFilter] = useState("");

    //Debounce hook
    const [results, setResults] = useState([]);
    // Searching status (whether there is pending API request)
    const [isSearching, setIsSearching] = useState(false);

    let token = localStorage.getItem("JWT").replace(/^"(.+(?="$))"$/, '$1');

    const [books, setBooks] = useState([]);

    const debouncedSearchTerm = useDebounce(filter, 500);

    const observable = useModalShow();
    const [breakPoint] = useBreakpoints();

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };

    useEffect(
        () => {
            console.log("Effect");
            fetch("http://localhost:8000/books", requestOptions).then((response) => response.json()).then((data) => {
                console.log("books fetch: ", data);
                setBooks(data);
            })
            if (fetchData) {
                fetch("http://localhost:8000/books", requestOptions).then((response) => response.json()).then((data) => {
                    console.log("books fetch: ", data);
                    setBooks(data);
                    setFectchData(false);
                })

            }

            if (debouncedSearchTerm) {
                console.log("debounce ")
                setIsSearching(true);
            } else {
                setResults([]);
                setIsSearching(false);
            }
            let timer1 = setTimeout(() => setShow(false), loadDelay * 1000);
            return () => {
                clearTimeout(timer1);
            };

        },
        [debouncedSearchTerm, fetchData]
    );

    //Shows info of books
    const handleClick = (pbook) => {
        console.log(book);
        console.log("handlebook: ", pbook);
        if (pbook === book) {
            setBook(null);
        } else {
            console.log("Setting book on click");
            setBook(pbook);
        }
    }

    const handleBook = (info) => {
        setShowModalBook(info);
    }

    const handleCloseModal = (info) => {
        setShowModalBook(info);
    }

    const submitBook = (book, modal) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` },
            body: JSON.stringify(book)
        };
        fetch('http://localhost:8000/books', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                console.log("Books post made: ", data);
                setShowModalBook(modal);
                setFectchData(true);
            });

    }


    if (token.length === 0 || token === null || books === "Session expired") {
        return (
            <div className="error__root">
                <h1>Not logged in</h1>
                <Link to={"/"}>Go home</Link>
            </div>
        );
    } else {

        return (
            <div>
                {show && <Modal addClose={false} show={show} />}

                {
                    //Show modal of add author
                    showModalBook === "Add book" &&
                    <Modal addClose={true} show={false} title={"Add book"} handleCloseModal={handleCloseModal} type={"book"} submitBook={submitBook} />
                }
                {
                    //Show modal of edit author
                    showModalBook === "Edit author" &&
                    <Modal addClose={true} show={false} title={"Edit author"} handleCloseModal={handleCloseModal} submitBook={submitBook} />
                }
                <div className="books__root">
                    {book && <div className="books__info__root">
                        <Outlet />
                    </div>}
                    <div className="books__info__btns">
                        <div className="books__breakpoints">
                            {breakPoint.length > 0 && breakPoint[0]}
                        </div>

                        <h2 className="books__title">Books</h2>
                        <AddInfo info={"Add book"} handleClick={() => handleBook("Add book")} bookClass={"add__btn--mod"} />
                        <Filter setFilter={setFilter} />
                        <ObjectList handleClick={handleClick} books={books} filter={filter} />

                        <GoBack type={"add__btn--mod"} />
                    </div>

                </div>
            </div>
        );
    }
}

// Hook
function useDebounce(value, delay) {
    // debounced value
    const [debouncedValue, setDebouncedValue] = useState(value);
    useEffect(
        () => {
            // Update debounced value after delay
            const handler = setTimeout(() => {
                setDebouncedValue(value);
            }, delay);
            return () => {
                clearTimeout(handler);
            };
        },
        [value, delay]
    );
    return debouncedValue;
}


export default Books;
