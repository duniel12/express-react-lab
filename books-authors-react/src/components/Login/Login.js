import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {

    const [formUserName, setFormUserName] = useState("");
    const [formPassword, setFormPassword] = useState("");
    const [error, setError] = useState(false);
    const navigate = useNavigate();

    const handleChangeInputs = (value, e) => {
        if (value === "username") {
            setFormUserName(e.target.value);
        } else if (value === "password") {
            setFormPassword(e.target.value);
        }
    }
    //Full submit
    let handleSubmit = (event) => {
        event.preventDefault();
        let loginInfo = { username: formUserName, password: formPassword };
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(loginInfo)
        };
        fetch('http://localhost:8000/users/login', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                console.log("Login made: ", data);
                
                if(data.error === "Invalid Password" || data.error === "User not found" || data.error === "Login not valid") {
                    setError(true);
                } else {
                    setError(false);
                    localStorage.setItem("JWT", JSON.stringify(data));
                    navigate("/authors");
                }
                
            });
    }

    return (
        <div className="login__root">
            <div className="login__cnt login__cnt--left">
                <div className="login__sub-cnt login__sub-cnt--left">
                    <i className="login__sub-cnt__icon"></i>
                    <h1 className="login__sub-cnt__title">Authors and books</h1>

                </div>
            </div>
            <div className="login__cnt login__cnt--right">
                <form onSubmit={(event) => { handleSubmit(event) }} className="login__sub-cnt login__sub-cnt--right">

                    <h2 className="login__sub-cnt__subtitle">Login</h2>
                
                    {error &&<div className="login__sub-cnt__error">
                        <p className="login__sub-cnt__error--text">Username and/or password invalid!</p>
                    </div>}
                    <div className="login__sub-cnt__cnt">
                        <label className="login__sub-cnt__lbl" htmlFor="login_username" >Username</label>
                        <input type={"text"} className="login__sub-cnt__inp" name="login_username" id="login_username" placeholder="d@gmail.com" value={formUserName || ""} onChange={e => handleChangeInputs("username", e)} />
                    </div>

                    <div className="login__sub-cnt__cnt">
                        <label className="login__sub-cnt__lbl" htmlFor="login_password">Password</label>
                        <input type={"text"} className="login__sub-cnt__inp" name="login_password" id="login_password" value={formPassword || ""} onChange={e => handleChangeInputs("password", e)} />
                    </div>


                    <button className="login__sub-cnt__submit" type="submit">Login</button>
                </form>
            </div>
        </div>
    );
}

export default Login;