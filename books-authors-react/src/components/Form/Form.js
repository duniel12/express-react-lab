import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';

const Form = (props) => {
    const { book, author, formType, submitAuthor,submitBook, handleCloseModal, type } = props;

    let newAuthor = {
        "fullName": "",
        "birthday": ""
    };
    if (author) {
        console.log("author")
        newAuthor.fullName = author.fullName;
        newAuthor.birthday = author.birthday;
    }

    let newBook = {
        "title": "",
        "author": "",
        "published_at": ""
    };
    if (book) {
        console.log("book")
        newBook.title = book.title;
        newBook.author = book.author;
        newBook.published_at = book.published_at;
    }

    //Author
    const [formAuthorName, setFormAuthorName] = useState(newAuthor.fullName);
    const [formAuthorBirthday, setFormAuthorBirthday] = useState(newAuthor.birthday);
    //Book
    const [formBookTitle, setFormBookTitle] = useState(newBook.title);
    const [formBookAuthor, setFormBookAuthor] = useState(newBook.author);
    const [formBookPublished, setFormBookPublished] = useState(newBook.published_at);

    let handleChangeAuthorDef = (value, e) => {

        if (value === "name") {
            setFormAuthorName(e.target.value);
            console.log("NAME: ", e.target.value)
        } else if (value === "birthday") {
            setFormAuthorBirthday(e.target.value);
            console.log("Birthday: ", e.target.value)
        }

    }
    let handleChangeBookDef = (value, e) => {

        if (value === "title") {
            setFormBookTitle(e.target.value);
            console.log("Title: ", e.target.value)
        } else if (value === "author") {
            setFormBookAuthor(e.target.value);
            console.log("Author: ", e.target.value)
        } else if (value === "published") {
            setFormBookPublished(e.target.value);
            console.log("Published: ", e.target.value)
        }

    }


    //Full submit
    let handleSubmit = (event, object) => {
        event.preventDefault();
        if (object === "author") {
            newAuthor = {
                "fullName": formAuthorName,
                "birthday": formAuthorBirthday
            };
            submitAuthor(newAuthor);
        } else if (object === "book") {
            newBook = {
                "title": formBookTitle,
                "author": formBookAuthor,
                "published_at": formBookPublished
            };
            submitBook(newBook);
        }
        handleCloseModal("no");
    }

    if (formType === "dynamic") {
        if(type === "author") {
            return (
                <form onSubmit={(event) => { handleSubmit(event, "author") }} className="form__root">
    
                    <div className="form__author__cnt">
    
                        <label for="author__name__inp">Name</label>
                        <input type="text" name="author__name__inp" className="form__input" id="author__name__inp" value={formAuthorName || ""} onChange={e => handleChangeAuthorDef("name", e)} />
    
                        <label for="author__nationality__inp">Birthday</label>
                        <input type="text" name="author__nationality__inp" className="form__input" id="author__nationality__inp" value={formAuthorBirthday || ""} onChange={e => handleChangeAuthorDef("birthday", e)} />
    
                    </div>
    
                    <div className="form__submit">
                        <button className="form__btn submit" type="submit">Submit</button>
                    </div>
                </form>
            )
        } else if(type === "book") {
            return (
                <form onSubmit={(event) => { handleSubmit(event, "book") }} className="form__root">
    
                    <div className="form__author__cnt">
    
                        <label for="book__title__inp">Title</label>
                        <input type="text" name="book__title__inp" className="form__input" id="book__title__inp" value={formBookTitle || ""} onChange={e => handleChangeBookDef("title", e)} />
    
                        <label for="book__author__inp">Author</label>
                        <input type="text" name="book__author__inp" className="form__input" id="book__author__inp" value={formBookAuthor || ""} onChange={e => handleChangeBookDef("author", e)} />

                        <label for="book__published__inp">Published at</label>
                        <input type="text" name="book__published__inp" className="form__input" id="book__published__inp" value={formBookPublished || ""} onChange={e => handleChangeBookDef("published", e)} />
    
                    </div>
    
                    <div className="form__submit">
                        <button className="form__btn submit" type="submit">Submit</button>
                    </div>
                </form>
            )
        }
    }

}

Form.propTypes = {
    author: PropTypes.object,
    book: PropTypes.object,
    formType: PropTypes.string.isRequired,
    submitAuthor: PropTypes.func,
    submitBook: PropTypes.func,
    handleCloseModal: PropTypes.func,
    type: PropTypes.string,
}

export default Form;