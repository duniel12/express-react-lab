import Object from "../Object/Object";
import PropTypes from 'prop-types';
import uuid from 'react-uuid';


const ObjectList = (props) => {
    const { handleClick, filter, authors, books } = props;
    //console.log(props);
    if (authors) {
        return (
            <ul className="object__info__list">
                {authors.filter(author => author.fullName.toLocaleLowerCase().includes(filter.toLowerCase())).map((author) =>
                    <Object key={uuid()} author={author} handleClick={handleClick} />
                )}

            </ul>
        );
    } else if (books) {
        return (
            <ul className="object__info__list">
                {books.filter(book => book.title.toLocaleLowerCase().includes(filter.toLowerCase())).map((book) =>
                    <Object key={uuid()} book={book} handleClick={handleClick} />
                )}

            </ul>
        );
    }
}

ObjectList.propTypes = {
    handleClick: PropTypes.func,
    filter: PropTypes.string,
    authors: PropTypes.array,
    books: PropTypes.array,
};

export default ObjectList;