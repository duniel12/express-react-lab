import React from 'react';
import PropTypes from 'prop-types';

const AddInfo = (props) => {
    const { info, handleClick, bookClass } = props;
    
    return (
            <button onClick={() => handleClick(info)}  className={`add__btn ${bookClass}`}>{info}</button>

    );
}

AddInfo.propTypes = {
    handleClick: PropTypes.func,
    info: PropTypes.string,
    bookClass: PropTypes.string,
};

export default AddInfo;
