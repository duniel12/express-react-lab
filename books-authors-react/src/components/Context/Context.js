import React, { useEffect, useState, createContext, useContext } from 'react';
import App from '../../App';

export const ApiContext = createContext({authors: null,books: null, isLoading: null});

const ApiContextProvider = () => {
    const [authors, setAuthors] = useState({});
    const [apiError, setApiError] = useState(null);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        fetch('http://localhost:3030/authors')
            .then(response => {
                if (response.ok) {
                    return response.json()
                }
                throw response;
            })
            .then(data => {
                console.log(data);
                setauthors(data.authors);
            })
            .catch(error => {
                console.error"(Error fetching data: ", error);
                setApiError(error);
            })
            .finally(() => {
                let timer1 = setTimeout(() => setLoading(false), 1.5 * 1000);
                return () => {
                    clearTimeout(timer1);
                };

            })
    }, []);

    return (
        <ApiContext.Provider value={{ authors: authors, isLoading: loading }}>
            <App />
        </ApiContext.Provider>
    );

};

export default ApiContextProvider;

export function useApiContext() {
    return useContext(ApiContext);
}

