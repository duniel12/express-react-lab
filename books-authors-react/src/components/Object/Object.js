import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'react-uuid';
import { Link } from "react-router-dom";

const Object = (props) => {
    const { author,book, handleClick } = props;
    
    if(author) {
        return (
            <li className="object__info__list--item" key={uuid()}>
                <Link onClick={() => handleClick(author)} key={author.id}  to={String(author._id)} className="object__info__list--li">
                    <button className="object__btn">{author.fullName}</button>
                </Link>
            </li>
    
        );
    } else if(book) {
        return (
            <li className="object__info__list--item" key={uuid()}>
                <Link onClick={() => handleClick(book)} key={book._id}  to={String(book._id)} className="object__info__list--li">
                    <button className="object__btn">{book.title}</button>
                </Link>
            </li>
    
        );
    }
}

Object.propTypes = {
    author: PropTypes.object,
    book: PropTypes.object,
    handleClick: PropTypes.func,
};

export default Object;
