import { Link } from "react-router-dom";
const GoBack = (props) => {
    const {type} = props;
    return(
        <div className="go-back__root">
            <Link to="/" className={`go-back__btn add__btn ${type}`}>Go home</Link>
        </div>
    );
}

export default GoBack;