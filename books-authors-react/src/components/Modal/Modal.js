import PropTypes from 'prop-types';
import AddInfo from '../AddInfo/AddInfo';
import Form from '../Form/Form';
import useModalShow from '../Hooks/useModalShow';
import React, { useState, useEffect } from 'react';
import { BehaviorSubject } from 'rxjs';


const Modal = (props) => {
    const {addClose, show, author, title, handleCloseModal, submitAuthor,submitBook, type} = props;
    const [showLocal, setShowLocal] = useState(true);
    const observable = new BehaviorSubject();


    if (show) {
        return (
            <div id="myModal" className="modal__root">
                <div className="modal__cnt modal__cnt--loader">

                    <p>Loading</p>
                    <div className="modal__loader"></div>
                </div>

            </div>
        );
    }
    else if (addClose) {
        return (
        <div id="myModal" className="modal__root">
            <div className="modal__cnt">
                {//<span className="modal__cnt__close">&times;</span>}
                }

                <button onClick={() => {handleCloseModal("no")}}>
                    &times;
                </button>

                <h2>{title}</h2>
                <Form author={author} formType={"dynamic"} submitAuthor={submitAuthor} submitBook={submitBook} handleCloseModal={handleCloseModal} type={type}/>
            </div>

        </div>
        );
    }


}

Modal.propTypes = {
    addClose: PropTypes.bool,
    show: PropTypes.bool.isRequired,
    author: PropTypes.object,
    handleCloseModal: PropTypes.func,
    submitAuthor: PropTypes.func,
}

export default Modal;