import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import Books from '../Books/Books';
import uuid from 'react-uuid';
import { useParams, Link } from 'react-router-dom';

const BooksInfo = (props) => {
    let { bookId } = useParams();
    const [isLoading, setIsLoading] = useState(true);
    const [book, setBook] = useState({});
    
    const [bookLoaded, setBookdLoaded] = useState(false);

    let token = localStorage.getItem("JWT").replace(/^"(.+(?="$))"$/, '$1');
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };

    const deleteBook = () => {
        if(bookId) {
            const requestOptions = {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
            };
            fetch(`http://localhost:8000/books/${bookId}`, requestOptions)
                .then((response) => response.json())
                .then((data) => {
                    console.log("Books deleted: ", data);
                });
        }
    }

    useEffect(
        () => {
            if(!bookLoaded) {
                console.log("se ejecuta");
            fetch(`http://localhost:8000/books/${bookId}`, requestOptions).then((response) => response.json()).then((data) => {
                console.log("book id fetch: ", data);
                setIsLoading(false);
                setBookdLoaded(true);
                setBook(data);
            })
            }
            

        },[bookLoaded]);

    if(isLoading) {
        return(<div>Loading</div>)
    } else {
        return (

            <div key={uuid()} className={`books__info__cnt books__info__cnt--${book.title}`}>
                <div className="books__info__cnt--flex">
                    <h2 className="books__info--title">Title</h2>
                    <p key={uuid()} className="books__info__name">{book.title}</p>
                    <h2 className="books__info--title">Published at</h2>
                    <p key={uuid()} className="books__info__nationality">{book.published_at}</p>
                    <Link className="delete__btn" to={"/books"} onClick={deleteBook}>Delete</Link>
                </div>  
            </div>
        );
    }
}

BooksInfo.propTypes = {
    name: PropTypes.string,
    nationality: PropTypes.string,
    books: PropTypes.array,
};

export default BooksInfo;