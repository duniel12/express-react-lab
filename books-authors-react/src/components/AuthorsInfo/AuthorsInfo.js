import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Books from '../Books/Books';
import uuid from 'react-uuid';
import { useParams, Link } from 'react-router-dom';

const AuthorsInfo = () => {
    let { authorId } = useParams();
    const [isLoading, setIsLoading] = useState(true);
    const [author, setAuthor] = useState({});
    const [authorLoaded, setAuthordLoaded] = useState(false);
    let token = localStorage.getItem("JWT").replace(/^"(.+(?="$))"$/, '$1');
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };

    const deleteAuthor = () => {
        if (authorId) {
            const requestOptions = {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
            };
            fetch(`http://localhost:8000/authors/${authorId}`, requestOptions)
                .then((response) => response.json())
                .then((data) => {
                    console.log("Author deleted: ", data);
                    setAuthor(null);
                });
        }
    }

    useEffect(
        () => {
            if (!authorLoaded) {
                console.log("se ejecuta");
                fetch(`http://localhost:8000/authors/${authorId}`, requestOptions).then((response) => response.json()).then((data) => {
                    console.log("Author id fetch: ", data);
                    setIsLoading(false);
                    setAuthordLoaded(true);
                    setAuthor(data);
                })
            }

        }, [author,authorLoaded, authorId]);

    if (isLoading) {
        return (<div>Loading</div>)
    } else {
        return (

            <div key={uuid()} className={` authors__info__cnt authors__info__cnt--${author.fullName}`}>
                <div className="authors__info__cnt--flex">
                    <h2 className="authors__info--title">Name</h2>
                    <p key={uuid()} className="authors__info__name">{author.fullName}</p>
                    <h2 className="authors__info--title">Birthday</h2>
                    <p key={uuid()} className="authors__info__nationality">{author.birthday}</p>
                </div>

                <div className="authors__info__cnt--flex">
                    <h2 className="authors__info--title">Books</h2>
                    <Books authorId={authorId} />
                </div>
                <div className="authors__info__cnt--flex">
                    <Link className="delete__btn" to={"/authors"} onClick={deleteAuthor}>Delete</Link>
                </div>


            </div>
        );
    }
}

AuthorsInfo.propTypes = {
    name: PropTypes.string,
    nationality: PropTypes.string,
    books: PropTypes.array,
};

export default AuthorsInfo;