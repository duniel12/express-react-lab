import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import uuid from 'react-uuid';

const Books = (props) => {
    const { authorId} = props;
    const [books, setBooks] = useState([]);
    let token = localStorage.getItem("JWT").replace(/^"(.+(?="$))"$/, '$1');
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };

    useEffect(
        () => {
            fetch("http://localhost:8000/books", requestOptions).then((response) => response.json()).then((data) => {
                console.log("Books fetch: ", data);
                let booksAuthor = [];
                data.forEach((book) => {
                    if(book.author === authorId) {
                        booksAuthor.push(book);
                    }
                })
                setBooks(booksAuthor);
            })

        },[setBooks]
    );

    if(authorId) {
        if(books.length > 0) {
            return (
                <ul>
                    {books.map((book) =>
                        <li key={uuid()}>
                            {book.title}
                        </li>
                    )}
                </ul>
            );
        } else {
            return(<p>No books</p>);
        }
    }
    
}

Books.propTypes = {
    books: PropTypes.array,
};

export default Books;