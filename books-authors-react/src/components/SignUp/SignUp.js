import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";

const SignUp = () => {
    const [formFullName, setFormFullName] = useState("");
    const [formUserName, setFormUserName] = useState("");
    const [formPassword, setFormPassword] = useState("");
    const [error, setError] = useState(false);
    const navigate = useNavigate();

    let handleInputChange = (value, e) => {
        if (value === "fullName") {
            setFormFullName(e.target.value);
        } else if (value === "username") {
            setFormUserName(e.target.value);
        } else if (value === "password") {
            setFormPassword(e.target.value);
        }

    }


    //Full submit
    let handleSubmit = (event) => {
        event.preventDefault();
        if (formFullName.length > 0 && formUserName.length > 0 && formPassword.length > 0) {
            
            let loginInfo = { fullName: formFullName, username: formUserName, password: formPassword };
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(loginInfo)
            };
            fetch('http://localhost:8000/users/signup', requestOptions)
                .then((response) => response.json())
                .then((data) => {
                    console.log("Post made: ", data);

                    if (data.error === "Invalid Password" || data.error === "User not found" || data.error === "Login not valid") {
                        setError(true);
                    } else {
                        setError(false);
                        navigate("/");
                    }

                });
        } else {
            setError(true);
        }

    }
    return (
        <form onSubmit={(event) => { handleSubmit(event) }} className="signup__root">
            {error && <div className="login__sub-cnt__error">
                <p className="login__sub-cnt__error--text">Invalid inputs</p>
            </div>}
            <h1>Sign up</h1>
            <div className="signup__info__cnt">

                <label htmlFor="book__fullName__inp">Full name</label>
                <input type="text" name="book__fullName__inp" className="signup__input" id="book__fullName__inp" value={formFullName || ""} onChange={e => handleInputChange("fullName", e)} />

                <label htmlFor="book__username__inp">Username</label>
                <input type="text" name="book__username__inp" className="signup__input" id="book__username__inp" value={formUserName || ""} onChange={e => handleInputChange("username", e)} />

                <label htmlFor="book__password__inp">Password</label>
                <input type="text" name="book__password__inp" className="signup__input" id="book__password__inp" value={formPassword || ""} onChange={e => handleInputChange("password", e)} />

            </div>

            <div className="signup__submit">
                <button className="signup__submit__btn submit" type="submit">Submit</button>
            </div>
        </form>
    );

}


export default SignUp;