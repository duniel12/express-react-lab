import { useState, useEffect } from "react";
import { BehaviorSubject } from 'rxjs';

let mystate$ = null;

const useModalShow = () => {
    const [show, setShow] = useState();
    if(!mystate$) {mystate$ = new BehaviorSubject(true)}

    useEffect(() => {
        const sub = mystate$.subscribe(setShow);
           return () => sub.unsubscribe();
    }, [show]);

    return {show};
};

export default useModalShow;

