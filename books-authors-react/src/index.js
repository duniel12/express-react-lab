import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Login from './components/Login/Login';
import SignUp from './components/SignUp/SignUp';
import AuthorsInfo from './components/AuthorsInfo/AuthorsInfo';
import BooksInfo from './components/BooksInfo/BooksInfo';
import Authors from './pages/Authors/Authors';
import Books from './pages/Books/Books';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
    <Routes>
      <Route path="/" element={<App />}>
      </Route>
      <Route path="/authors" element={<Authors />}>
        <Route path="/authors/:authorId" element={<AuthorsInfo />} />
      </Route>
      <Route path="/books" element={<Books />}>
        <Route path="/books/:bookId" element={<BooksInfo />} />
      </Route>
      <Route path="/login" element={<Login />} />
      <Route path="/signup" element={<SignUp />} />

    </Routes>
  </Router >
)

reportWebVitals();
