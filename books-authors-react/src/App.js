//import logo from './logo.svg';
import React, { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';


import './components/style.scss';



function App() {

  return (
    <div className="main__root">
      <div className="nav__root">
        <header className="nav__header">
          
          <nav className="nav__cnt" id="sidebar-menu">
            <ul className="nav__cnt__links">
              <li>
                <Link className="nav__links" to={"/login"}>Login</Link>
              </li>
              <li>
              <Link className="nav__links" to={"/signup"}>Sign up</Link>
              </li>
            </ul>
          </nav>
        </header>
      </div>
      <div className="info__cnt">

        <Link className="info__cnt-side info__cnt-left" to={"/authors"}><p className="info__cnt-side__text">Authors</p></Link>
        <Link className="info__cnt-side info__cnt-right" to={"/books"}><p className="info__cnt-side__text">Books</p></Link>
      </div>
      <div className="overlay"></div>
    </div>
  );
}

export default App;
