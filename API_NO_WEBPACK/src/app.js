const express = require("express");
const mongoose = require("mongoose");

const bp = require('body-parser');


const loggerMiddleware = require("./middleware/logger.middleware");
const postMiddleware = require("./middleware/content-type.middleware");
const errorMiddleware = require("./middleware/error.middleware");
const authenticateTokenMiddleware = require("./middleware/authenticate-token.middleware");

const app = express();


const DB_HOST = "localhost:27017";
const DB_NAME = "immersive-lab";

mongoose
  .connect(`mongodb://${DB_HOST}/${DB_NAME}`)
  .then(() => {
    console.log(`Connection to ${DB_HOST}/${DB_NAME} opened`);
  })
  .catch((err) => {
    console.log("Error connecting to mongo: ", err);
  });

const authorsRouter = require("./routes/authors.route");
const booksRouter = require("./routes/books.route");
const usersRouter = require("./routes/users.route");
const cors = require("cors");
app.use(cors());

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

//Custom middlewares
app.use(loggerMiddleware.loggerHandler);
app.post("/*",postMiddleware.contentHandler)

app.use("/users", usersRouter)
app.use("/authors",authenticateTokenMiddleware.authenticateToken,authorsRouter);
app.use("/books",authenticateTokenMiddleware.authenticateToken, booksRouter);


app.use(errorMiddleware.errorHandler);
//console.log(require('crypto').randomBytes(64).toString('hex'));

module.exports = app;